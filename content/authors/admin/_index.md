---
# Display name
# title: Nelson Bighetti

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
# role: Professor of Artificial Intelligence

# Organizations/Affiliations
# organizations:
# - name: Stanford University
#   url: ""

# Short bio (displayed in user profile at end of posts)
# bio: My research interests include distributed robotics, mobile computing and programmable matter.

# interests:
# - Artificial Intelligence
# - Computational Linguistics
# - Information Retrieval

# education:
#   courses:
#   - course: PhD in Artificial Intelligence
#     institution: Stanford University
#     year: 2012
#   - course: MEng in Artificial Intelligence
#     institution: Massachusetts Institute of Technology
#     year: 2009
#   - course: BSc in Artificial Intelligence
#     institution: Massachusetts Institute of Technology
#     year: 2008

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:thecontruthversial@protonmail.ch'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/contrueversial

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
# user_groups:
# - Researchers
# - Visitors
---


- Have a controversial opinion that you'd like to share, but afraid of revealing your identity for fear of repercussions? 
- Tired of censorship by your government or ISP?
- Annoyed when your well-meaning posts get deleted by moderators on other platforms?

We provide you with a safe space so that you're free to express your thoughts without worry.


## How does this work?

1. Send us your draft post via email, in the following format:
    ```
      Subject: Title of your post / article
      Body:

        - Do you want to be anonymous? (Yes/No)
        - Content:
          ... // content of your post
    ```
2. We will let you know once our editorial team starts to look into your draft. During this process, our editors will go through your piece and do a thorough fact-checking.
3. Once we're happy with the piece, we will let you know that its ready to be published.
