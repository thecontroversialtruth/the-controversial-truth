+++

title = "Our Manifesto"
subtitle = ""

date = "2020-04-21"

authors = ["Admin"]
categories = [
  "Internet Activism",
]

tags = [
  "the controversial truth",
  "values",
  "censorship",
  "digital privacy"
]
+++

We're a small group of independent thinkers and internet activists. Our manifesto is quite simple, really, and we operate on these priniciples

### Principle #1: Anonymity first.

That means no tracking your cookies, no showing ads, none of that bullshit. We don't store user accounts. All posts are anonymous here, unless the author specifically requests for their name to be shown.

### Principle #2: Freedom of Speech

We believe that you have a right to express your thoughts. That means no censoring. We won't alter or change any of the content here - except when something is shown to be factually incorrect, and only after explicitly consulting the author.

### Principle #3: No Hate Speech

Freedom of speech doesn't mean that we think saying _the holocaust was a good thing_, or that _gays should be killed_ is a good thing. Nope. We won't have any of that shit here.

### Principle #4: Evidence based

We care only about facts. And yes, we do understand that sometimes it can be a bit of a grey area. But we believe in doing due diligence - especially if its controversial.

Do you think transgender women shouldn't be allowed to compete in women sports, and you have a strong reason for it? We're all ears. Are you a flat-earther? Nope, we won't be putting up with that.

### Principle #5: Error correcting

We don't believe in tying our opinions with our identities. Opinions can change. Sometimes we might get something wrong. And that's okay - we're human after all.

But we're not trying to be perfect. What matters is the ability to self-correct. If one of the posts on our platform turn out to be wrong, show us the evidence for it. Write to us. We're more than happy to rectify it. Because we believe in learning from our mistakes.

<br> 

## Want to contribute?

Write us an email at `thecontruthversial@protonmail.ch`