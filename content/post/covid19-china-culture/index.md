+++

title = "Covid-19, China and Culture. What do these three have in common?"
draft = false

authors = ["Anonymous"]

categories = [
  "Culture",
]

tags = [
  "China",
  "COVID-19"
]

date = "2020-04-27"
+++

### I think that the current outbreak of the Coronavirus has a lot to do with cultural practices and beliefs, and that China must be held at least partly responsible for the COVID-19 Pandemic.

I know that this will come across as racist. But you know what? I don't care. I don't care if the so-called "woke" people's fragile feelings get hurt.

Why the hell should a culture's practices be immune to scrutiny and criticism anyway? When entire fields like medicine, astronomy, mathematics, physics, and even politics - are open to critical thinking, analysis and error-correction, then what makes some people think that somehow no matter what a cultural/religious practice might be, it should just be "accepted", "respected" and "tolerated"?

If speaking the truth involves pointing out problems in a certain culture's practices and beliefs, and all you can do is shoo it away by saying "you're a racist", then **you too, are a part of the problem**.


---

Now, let's come to my actual opinion : ** Why is China partly responsible?**

Actually let me rephrase: **China is majorly responsible**.

**Two major reasons:**

1. China's belief system of "Jinbu".
2. China's culture has an extremely long history of punishing the messenger and shrouding the news in secrecy when the message is bad news.

And I will break these points down, one by one. It's going to be a fact-filled rant, so be prepared. Ready? Let's go!

<br>

## Reason #1: China's belief system of "Jinbu".

What is Jinbu? It is a traditional Chinese cultural belief that by eating certain kinds of animals, you'll fill up gaps in the energy systems in your body. These animal parts are apparently some kind of "aphrodisiac", and eating them and their body parts (and often, raw!) will give you more sexual prowess, cure erectile dysfunction and make you look beautiful.

Mind you, this isn't related to any actual biological concept, but is based on some magic. Sounds very much like witchcraft right? There's also the belief that that eating animals of a certain shape, will strengthen your organs which resemble that shape. Now if you have the stomach for it, here are some videos below. Look at the animals in the videos and take a guess - what part of the human body do they resemble? (Hint: It rhymes with SICK)

{{% alert warning %}}
EXTREME GRAPHIC CONTENT AHEAD
{{% /alert %}} 

<br>

#### Eating Snakes Hot Pot
{{< youtube YHjj-V8_LT4 >}}

<br>

#### How about the penis of a bull?
{{< youtube xwxdI1XSQDU >}}

{{% alert note %}}
Spoiler alert: it is being prescribed by a Chinese medical professional! These bull penises are sold at many pharmacies as Chinese traditional medicine.
{{% /alert %}}


[China's Niche Market for Bull Penis Is Small But Powerful](https://www.vice.com/en_us/article/8qkj53/chinas-niche-market-for-bull-penien-china-el-mercado-de-pene-de-toro-es-pequeno-pero-poderosos)

<br>

#### [Tiger bones and Rhino horns](https://www.dw.com/en/chinas-medicinal-tiger-bones-and-rhino-horns-tradition-or-travesty/a-46193315)

<br>

#### Want some more?
The belief goes further  -  they believe that if an animal is tortured, then the benefits of this "Jinbu" will be more, as the animal will be full of energy and adrenaline before it is finally eaten. Here's a footage of a dog being cooked **ALIVE**

{{< tweet 1241068148167671808 >}}

Every year, thousands of dogs and cats are slaughtered during the Yulin Dog festival that happens in Southwest China. Many of them are freaking [BOILED ALIVE](https://www.mirror.co.uk/news/world-news/inside-horrors-yulin-dog-festival-17264832), again - because of the belief that the meat tastes better.

#### Bats, Civets and other wildlife animals

How did the **SARS** outbreak happen in the early 2000? Scientists traced back the virus to bats and this animal -
![](https://qphs.fs.quoracdn.net/main-qimg-38ba77e93175ba0b9e8ef5185b3cf882)
It's called a Civet. Both bats and civets are somewhat of a delicacy in **Southwest China**.

Bats are a major carrier of dangerous viruses and pathogens. Again, they are eaten because of the Jinbu belief that eating bats can [restore eyesight](https://www.bestplant.shop/products/ye-ming-sha-bat-feces-bat-dung-bat-guano), and in particular - eating their **feces/excreta**. Wanna know why? Because their shit sparkles in the dark!

![](https://qphs.fs.quoracdn.net/main-qimg-36cdb893051f0ac1b480b44642dcd6e7)

> First the skin, now the poop too? Get me off this planet!


**Gives a whole new meaning to the term batshit crazy doesn't it?**


These cultural practices have lead rise to [wildlife trade](https://www.bbc.com/news/science-environment-51310786) in China. It's also the [origin of the Coronavirus](https://www.youtube.com/watch?v=TPpoJGYlW54). And their government has done little to stop them.

Not to mention, we have [epidemiological evidence](https://www.politico.com/news/magazine/2020/03/07/coronavirus-epidemic-prediction-policy-advice-121172) as well. Epidemiologists and bio-statisticians have been saying from years that a viral pandemic was in-fact long overdue, and the next outbreak was going to happen very likely in China.


---

Moving on to -

## Reason #2: China's culture has an extremely long history of punishing the messenger and shrouding the news in secrecy when the message is bad news.

Take for example the case of SARS. The first outbreak was reported in China on November 2002. Despite the diagnosis, Chinese government officials did not inform the WHO about the outbreak until February 2003, 4 freaking months later. The surgeon who first reported the news was held in [military detention](https://www.nytimes.com/2004/07/21/world/china-releases-the-sars-whistle-blower.html) for 45 days!

China later [apologized](https://www.baltimoresun.com/bal-te.sars21apr21-story.html) for it, but what's the point when you can't learn from your own mistakes? Now we have COVID-19. 
[Dr. Li Wenliang](https://time.com/5779678/li-wenliang-coronavirus-china-doctor-death/), 34 who first sounded the alarm was reprimanded and detained by Chinese officials for "spreading rumours". And now he's dead.

Want another example? Read this story of the [milk scandal](https://www.forbes.com/sites/yanzhonghuang/2014/07/16/the-2008-milk-scandal-revisited/#575aaadf4105) in 2008, when more than 300,000 babies fell ill, and then the person who revealed this news was stabbed 4 years later.

Then again, we have the very infamous [The 1957 Anti-Rightist Movement](https://www.ejinsight.com/eji/article/id/1581700/20170609-the-1957-anti-rightist-movement-a-warning-from-history). Thousands of people were jailed, punished and their families destroyed for criticizing the Communist party's mistakes.

**Those who forget and ignore history, are condemned to repeat it, amirite?**


---

Now you might say - 

> "Okay, but this is all the handiwork of the Chinese Communist Party. The Chinese themselves are the victims. What does it have anything to do with culture?"

Yes it does. It absolutely f***ing does. Sure, the Chinese are also amongst the victims, **but of their own culture**. It's biting them. The communist party is just the instrument of the same Chinese culture that likes to keep things shrouded in secrecy and punish whisleblowers. It's not a modern invention  -  it has been the case at-least since the time of Confucius in the 6th century BC.


## The Cultural Background:

The death penalty is one of the classical five punishments of the Chinese dynasties that began from _Yu The Great_ from 2070 BC and lasted upto 1912  -  almost 4 millenia! Although there are no more dynasties, the capital punishment has [widespread public support](https://en.wikipedia.org/wiki/Capital_punishment_in_China#Public_support) in China. More people are executed or sentenced to death in China than in the rest of the world put together  -  a record of 2400 executions in 2013. (And it isn't even the official number).

But here is where things get interesting -

A total of about 46 non-violent crimes "against the state" are subject to capital punishment. And until November 2015, amongst those crimes was this one  -  "[fabricating rumors to mislead others during wartime](http://www.worldcoalition.org/China-reduces-the-number-of-crimes-punishable-by-death-to-46-but-keep-drug-trafficking-in-the-list.html)". This law got amended to the following:

> Modify Criminal Law article 433 to read: "Those spreading rumors to confuse the public or shaking the moral of the military during wartime are sentenced to up to three years imprisonment; where circumstances are serious the sentence is between three and ten years imprisonment; where circumstances are especially serious the sentence is ten or more years imprisonment or indefinite imprisonment.

Now that by itself might seem benign  -  but consider how that could easily be used to justify keeping whistleblowers like Dr. Li under detention. After all, what exactly can be constituted as rumour? This can easily be used to [curtail freedom of speech](https://www.hrw.org/news/2015/11/02/china-new-ban-spreading-rumors-about-disasters)  -  and you've already seen the evidence for it! Not to mention that prior to this amendment in 2015, you could be killed!

Think again  -  what allowed this to happen?


---

**Now, some common counter-arguments that people often cite:**

### Argument #1: You're a hate monger and a racist

To that, my response would be  -  LOL. If you can only come up with an [ad-homenim](https://en.wikipedia.org/wiki/Ad_hominem) attack, then don't bother because you've already lost.

### Argument #2: Such cultural practices of eating dogs, cats, bats are also present in other countries like Indonesia. How can you hold only China responsible?

For that I will respond with this  -  I never said "only China". But China is responsible because it had the opportunity to learn from its own mistakes unlike other countries. It is responsible for the spread of the Coronavirus and SARS. It also perpetuates a culture of secrecy, a culture of silencing critics, and very few countries have wildlife trade farming to the scale and magnitude that China has.

[Chinese traditional medicine](https://en.wikipedia.org/wiki/Traditional_Chinese_medicine#Animal_substances) has concoctions involving animal ingredients, which are being taught in textbooks. Some of those medicines involve animal organs like Rhino horns - which is responsible for the world's rhino population to decline by more than **90%**.

### Argument #3: People consume meat from cows, chickens, fish, pork all over the world. You didn't say anything about those countries, but you're only targetting China.

For this my response would be:

- Yes I'm aware of that. But you won't find a dog being f***ing blowtorched or boiled alive in these countries. That is not to say that their methods are very humane, I know that chickens / goose / cows also live miserable lives in the slaughterhouse. But at-least they're not being cooked alive.

- Moreover, this meat don't carry the kind of pathogens and viruses that bats and other wildlife animals carry. Have you ever heard of any recent pandemic outbreak from anywhere else?

You do realize that the suffering of an animal is related to its levels of [self-awareness](https://en.wikipedia.org/wiki/Animal_consciousness#Pain_or_suffering)? There is a big difference between eating a chicken and eating a dog. Dogs have been bred by humans for millennia that they now have strong emotional bonds with humans. And when you kill them - much less boil them alive - you're promoting needless suffering.

I'm by no means a human-rights activist or a vegan, and I know that you can't completely eliminate suffering everywhere. But you can certainly try to reduce it, yes?

### Argument #4: Not all Chinese people eat bats and indulge in these practices.

Sure, not all. Reminds me of #NotAllMen. But consider this:

- Majority of the Chinese are still fairly tolerant of their countrymen who do indulge in these practices. How else do you explain the fact that wildlife trade of such magnitude is still a thing there, after so many years?
- Even if a small percentage of people indulge in these practices, it is still enough to wreak havoc on the entire world!

A problem is a problem however small, making a hashtag like #NotAllChinese or something like that doesn't make it go away you know? Understand that these practices are normalized, their public has become numb, and their traditional medicine systems have recipes that involve animal parts.


### Argument #5: But shouldn't we still respect someone else's culture? It is, after all, part of their beliefs and traditions.

**Short answer - No**. Nobody gets to demand respect  -  why should they? You only respect something that's respectable. And people who say this are confusing respect with kindness. I'm not saying that we can't treat someone with **kindness** or civility - we absolutely should.

**Long answer:**

This very idea of tolerance no matter what  -  is a dangerously misguided philosophy. By relaxing our very own standards of adherence to rationality, human & animal rights, medicinal systems and pretty much anything else in the name of "tolerance", we make it much harder to even acknowledge the problems that in exist in many cultures, religions and traditions. In the name of "political correctness", we make it taboo to even have a rational discussion.

Not to stray away from the point, but the recent Tablighi Jamaat incident in India is also another example.

It really boils down to two simple things:

- Does the practice violate human or animal rights or create harm in any way?
- Is it because the culture endorses that practice (knowingly or unknowingly)?

If these two are true, and there are significant number of followers of that culture who indulge in such practices, then the problem lies within that **culture**, not the people. It doesn't matter whether that culture was "misinterpreted" or not, or whether it "intended" to be that way or not. It really doesn't. You have a f***ing moral obligation to point it out - so that that it can either be error-corrected, or go away. It doesn't mean that you are, or have to be, a racist or a "phobic", as some pseudo-liberals would like you to believe.

But if you remain silent under the guise of tolerance, that will be a grave mistake that you and your future generations are going to come to deeply regret.

Still not convinced? Let me pick a more extreme example: Would you be respectful or tolerant of a culture that practices slavery? How about cannibalism?

Due to globalization, we can no longer ignore and turn a blind eye anymore. Cultural or religious practices in some corner of the world have the potential to affect the lives of millions of people pretty much anywhere in the world - as we're experiencing now.

### Argument #6: Do you have anything positive to say about the Chinese?

Why, you think I hate them? I told you it was going to be a rant!

No I don't hate them  -  I have nothing against the Chinese people. I do think of them positively in general. I LOVE Jackie Chan's movies. And I think that Made in China's products are awesome. I love China's Xiaomi mobile phones. I think that the Chinese are excellent programmers. I think they're largely a very self-reliant country.

And good for finally handling COVID-19 the way it's supposed to be handled.

## But please China, get your shit together. And no, not of the bat kind.

Thanks for reading. My only request is don't use this post to insult the Chinese people. Rather, it should be used to understand what is essentially a multi-faceted problem, and work together.


## Citations:

* https://www.vice.com/en_us/article/8qkj53/chinas-niche-market-for-bull-penien-china-el-mercado-de-pene-de-toro-es-pequeno-pero-poderosos
* https://www.dw.com/en/chinas-medicinal-tiger-bones-and-rhino-horns-tradition-or-travesty/a-46193315
* https://www.mirror.co.uk/news/world-news/inside-horrors-yulin-dog-festival-17264832
* https://www.bestplant.shop/products/ye-ming-sha-bat-feces-bat-dung-bat-guano
* https://www.bbc.com/news/science-environment-51310786
* https://www.politico.com/news/magazine/2020/03/07/coronavirus-epidemic-prediction-policy-advice-121172
* https://www.nytimes.com/2020/02/20/opinion/sunday/coronavirus-china-cause.html