+++

title = "About this publication"
subtitle = ""
draft = false
date = "2020-04-21"

authors = ["Admin"]

categories = [
  "Internet Activism",
]

tags = [
  "the controversial truth",
  "values",
  "censorship",
  "digital privacy"
]

keywords = [
  "the controversial truth",
  "values",
  "censorship",
  "digital privacy"
]
+++
# Preface
Over the last several decades, the world has seen a rise in technological innovation like never before. Birth control, free press, and a systemic re-evaluation of what it means to believe in human rights. We used to believe that gays should be punished. Transgenders should be outcasted. Capital punishment is necessary and good. Atheists should be condemned. And if you're colored in any other shade other than white, then you're inferior.

And we was quite mistaken, quite terribly. The old rules were wrong. When we realized that, we went back to our books, and made new rules. Rules that would hopefully be more inclusive, and usher in a new age where notions of right and wrong would no longer be muddied by our biases.

## And yet, nothing - absolutely nothing - could prepare us for what the internet would turn out to be.

The Internet, for some, is nothing more than a never ending stream of cute cat videos, music from The Weeknd, and pictures of Emma Stone. And perhaps Netflix as well. But for others, it's more. With its own share of positive impacts on the world, the digital space has brought about its fair share of evils too.

The World Wide Web is a gigantic cohesion of thoughts, a great leveller. Where the rich and the poor influence each other. It's a chaotic place - a world where truth and lies look alike. It's a place where the true voices of reason can get drowned, amongst screams of agents masquerading as liberals and torch-bearers. It's no wonder why entire world governments want to get absolutely every bit of control that they can get over it.

And it brings with it problems. Real problems that can influence your very thoughts, beliefs and your very outlook of the world:

### Problem #1: Censorship

Perhaps one of the most extreme forms of censorship that most people are familiar with, [can be seen in North Korea](https://medium.com/swlh/8-days-in-north-korea-5c651c3883de) - a modern kingdom, that is arguably in isolation with the rest of the world.

Government censorship is getting stronger than ever. And you've seen time and time again, that censorship has negative consequences far more deadlier than the evil that it touts to prevent. The recent COVID-19 pandemic is a stark example of what even a couple of months of information hiding in one regime of the world can do to entire so-called "superpowers" across the world.


### Problem #2: Content moderation and distribution

Censorship comes in an another form as well, in a benign, often innocent form - **content moderation**. Now I don't think moderation is necessarily bad - it has an important place on social media platforms, and it serves its purpose. But we've seen, from many examples before, that moderation is highly subjective to the whims of the moderator.

Platform rules as to what exactly constitutes "offensive", "trolling", and "misleading" are often highly arbitrary. Content moderation isn't decided on the basis of merit or facts - rather, its more or less decided by how many people get offended.

Distribution algorithms are designed to feed you the most sensationalized news on the internet. News that is tailored to your cognitive biases, political opinions and beliefs. In this process, truth often gets submerged in a sea mixed with half-truths, rumours and opinions.

### Problem #3: Anonymity and privacy 

Your data and privacy are at an ever-increasing risk of getting compromised. With the advent of social media and internet governance, your Facebook or twitter profiles are no more seen as isolated from yourself. Digital artifacts are not viewed as any less real than physical ones. Rather, your social media profiles are seen as extensions of yourself - your very real identities.

Unlike your physical identity, your social media identity is readily accessible to the world, 24/7. It can be used by third parties to manipulate you and your close ones. It can be sold. And it will continue to exist even after your physical body doesn't.

As Mark Manson puts it - [we're living in the age of outrage](https://markmanson.net/outrage). Your tweet or make a facebook post, however factual it may be - if seen as offensive or misinterpreted, has the potential to end your career. And that's just if you're lucky. It could be worse. People have faced death threats, simply for expressing facts or having a different opinion.


### Problem #3: Radical Ideologues

Just like social identities, people's political opinions are very much tied to their identities. One particular problem that we find extremely disturbing is the ideologues of the **radical right** and the **radical left**.

The radical right operate under the guise of nationalism. With these kind, you can see them perpetrating a false kind of patriotism, pseudoscience, and in some cases - a gross violation of trust and modern human rights.

The radical left perpetrates [this idea](https://youtu.be/3-SGqRToXYE) that the ones that win are oppressors, and the ones who lose are oppressed. They're also notorious of promoting "class-based" guilt. 


But the biggest danger that I see is - the radical left often indulge in silencing valid criticisms of a particular group or culture, under the guise of "inclusiveness" and "civil rights". What's funny and ironic is - that these groups that the radical left come to defend, have some of the most dangerous and radical right-wing ideas.

It's all bullshit, really. I don't buy any of that.

What matters is the truth, and the truth alone. And while the truth can be a bitter pill to swallow, that is the only thing that can save us - unlike believing in fairy tales and happy endings.


# And that is why we are here.

We are internet activists, amongst many other things. We want to provide a space for you to rant whatever it is that you always wanted to say, but were afraid to say it out loud. No, that doesn't mean that we endorse hate speech - they're not the same thing.

But we do support **factual criticism**. In-fact, we highly encourage it. As [Jordan Peterson](https://youtu.be/aMcjxSThD54) would put it - 

> In order to be able to think, you have to risk being offensive in pursuit of the truth.

Feel free check out [our Manifesto](/post/our-manifesto).